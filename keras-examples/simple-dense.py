from keras.models import Model
from keras.layers import Dense, Input
import numpy as np
import os, sys

sys.path.insert(1, os.path.join(sys.path[0], '..'))
from utils.DenseDebug import *

#currently code is implemented for keras1

x = np.arange(20).reshape(-1,2)
y = (np.random.rand(10,2) > .5) * 1

inp = Input(shape=(2,))
ddlayer1 = DenseDebug(10)
den = ddlayer1(inp) 
print("after dense layer 1 is created")
print("trainable_weights is")
print(ddlayer1.trainable_weights)
out = DenseDebug(2, activation='sigmoid')(den)
print("after dense layer 2 is created")
mdl = Model(inp, out)
mdl.compile(optimizer='rmsprop', loss='binary_crossentropy')
print("about to run train")
mdl.fit(x,y, nb_epoch=4)
print("about to run predict")
out = mdl.predict(np.arange(20).reshape(-1,2))

print(out)

'''
Second attempt at implementing a custom layer for Diet Networks.
See also TieOutToWeights

Strategy
1. review Dense layer code
2. review merge.py layers.
3. implement something like DenseWeightsFromTensor(100)([model_out_tensor, inp]) which is just one dense layer 
   which uses the out_tensor as it's weights.
'''

class DenseWeightsFromTensor(Layer):
  
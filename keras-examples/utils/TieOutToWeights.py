from keras.layers import Dense
from keras import backend as K
from keras.engine.topology import Layer

#usage: TieOutputToWeights(Dense(10), Dense(20))

#left off here. Some ideas
'''
* Wrap one Dense layer (not two).  Use lower level keras.backend functions to implement the second (or first) Dense Layer
* Extend the Layer class instead of the layer class and wrap two Dense Layers. 
* review attentional model code. https://github.com/fastai/courses/blob/master/deeplearning2/attention_wrapper.py
* I think this is useless but you might be able to just creates two dense layers when given an input and messes with
 one of their kernels after the fact. https://stackoverflow.com/questions/44077892/how-can-i-reuse-a-composite-keras-layer
* see the simple-dense.py class.  You'll get some info on when functions get called. 

'''

#NOTE: you can't extend Wrapper here unless you take one layer in your constructore
class TieOutputToWeights(Wrapper):

    #taken from keas sample custom layer code. 
    def __init__(self, output_dim, **kwargs):
        self.output_dim = output_dim
        super(MyLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      shape=(input_shape[1], self.output_dim),
                                      initializer='uniform',
                                      trainable=True)
        super(TieOutputToWeights, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        return K.dot(x, self.kernel)

    #My pseudo code: 
    def call(self, x):
        out = dense1.call()
        dense2.trainable_weights[0] = out #this might be wrong.  Technically I think dense2's weights are not trainiable. 
        dense2.call() 
        

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)
